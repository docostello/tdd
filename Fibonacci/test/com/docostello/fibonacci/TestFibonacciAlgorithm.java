package com.docostello.fibonacci;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestFibonacciAlgorithm {

	@Test
	public void testFibonacci() {
		int position = 4;
		Fibonacci fib = new Fibonacci();
		int result = fib.getNumber(position);
		if(result<0)
		{
			assertFalse(result<0);
		}
		else
		{
			if(position==1)
			{
				assertTrue(result==0);
			}
			else if(position==2 || position==3)
			{
				assertTrue(result==1);
			}
			else
			{
				System.out.println(result);
				assertTrue(result == fib.getNumber(position-1)+fib.getNumber(position-2));
			}
		}
	}

}
