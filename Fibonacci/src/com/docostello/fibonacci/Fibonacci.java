package com.docostello.fibonacci;

public class Fibonacci {
	public int getNumber(int position)
	{
		int result = 0;
		if(position<0)
		{
			result = -1;
		}
		else if(position==1)
		{
			result = 0;
		}
		else if(position==2 || position==3)
		{
			result+=1;
		}
		else
		{
			result = (getNumber(position-1) + getNumber(position-2));
		}
		return result;
	}
}
