Daniel Costello
Agile Software Development
02/02/2015

TDD Summary

After developing three programs using TDD techniques, I believe it is very beneficial and I had a good experience with it.  I thought there were many advantages. First, it forces the developer to think through the requirements before the development process starts. The requirements must be understood and clear in the developers mind in order to write the tests. It forces the developer to think through the entire problem before they start actually developing the application. 

The next advantage is that the code is extremely maintainable.  After the development of the TicTacToe application I started refactoring the code and the application did not pass the unit test anymore. I looked at the change and realized the reason it failed and then fixed the problem. This is a massive advantage to TDD. If TDD were not used in this situation, I would have introduced a new error in to the application. In that sense, TDD provides a confidence to the developer and allows them to clean the code without worrying if they broke the code or not. They can easily run the unit test and know if it broken or not. 

It can be used as a safety net when it comes to adding new functionality and business logic into a application. The developers can add the logic and check if the application still passes the unit test. If it does not, then there is more work to be done. The tests can tell the developers if the code is finished or not. 

One disadvantage to TDD was that while I was developing the application I felt like I was tied to the unit test. I wanted to structure the code in a way that would pass the test. There may have been better ways to structure the code but I based the structure off of the test that was initially written. I believe this would drastically improve with experience developing using TDD techniques. 

I believe I am going to try and start implementing some TDD techniques on some of my smaller projects. I want to start with smaller projects because it is easier to think through all of the requirements. After I gain some experience with TDD, I will then start to implement it in bigger projects. I really like the confidence is provides the developer and this is my main reason for wanting to start using TDD techniques. The ability to instantly tell if the code in a application is broke or not is a massive advantage. 

