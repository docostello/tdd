package com.docostello.tictactoe;

import static org.junit.Assert.*;

import org.junit.Test;

import com.docostello.tictactoe.TicTacToe.Player;

public class TicTacToeTest {

	@Test
	public void test() {
		TicTacToe t = new TicTacToe();
		t.initBoard();
		char[][] board = t.displayBoard();
		for(int i=0;i<3;i++)
		{
			t.insertPiece(Player.human, i, 0);
		}
		Player player = t.isWinner(Player.human);
		assertTrue(player==Player.human);
		t.clearBoard();
		for(int i=0;i<3;i++)
		{
			board[i][i] = t.humanMark;
		}
		player = t.isWinner(Player.human);
		assertTrue(player==Player.human);
		boolean isDraw = t.isDraw();
		assertTrue(isDraw==false);
	}

}
