package com.docostello.tictactoe;

public class TicTacToe {
	private char[][]board;
	public char humanMark = 'X';
	public char computerMark = 'O';
	public TicTacToe()
	{
		board = new char[3][3];

	}
	enum Player
	{
		none,
		human,
		computer
	}
	public void initBoard()
	{
		for(int i=0;i<3;i++)
		{
			for(int x=0;x<3;x++)
			{
				board[i][x] = '-';
			}
		}
	}
	public char[][] displayBoard()
	{
		System.out.println("-----------------");
		for(int i=0;i<3;i++)
		{
			System.out.print("| ");
			for(int x=0;x<3;x++)
			{
				System.out.print(board[i][x] + " | ");
			}
			System.out.println();
		}
		System.out.println("-----------------");
		return board;
	}
	public boolean isDraw()
	{
		boolean isDraw = true;
		for(int i=0;i<3;i++)
		{
			for(int x=0;x<3;x++)
			{
				if(board[i][x]=='-')
				{
					isDraw = false;
				}
			}
		}
		return isDraw;
	}
	public Player isWinner(Player player)
	{
		char mark = '-';
		if(player==Player.human)mark=humanMark;
		if(player==Player.computer)mark=computerMark;
		Player isWinner = Player.none;
		for(int i=0;i<3;i++)
		{
			if(board[i][0] == mark && board[i][1] == mark && board[i][2]==mark)
			{
				isWinner = player;
			}
			if(board[0][i] == mark && board[1][i] == mark && board[2][i] == mark)
			{
				isWinner = player;
			}
			if(board[0][0] == mark && board[1][1] == mark && board[2][2] == mark)
			{
				isWinner = player;
			}
		}
		return isWinner;
	}
	public void insertPiece(Player player,int row,int column)
	{
		if(row>=0 && row<3)
		{
			if(column>=0 && column<3)
			{
				if(board[row][column]=='-')
				{
					if(player == Player.human)
					{
						board[row][column]=humanMark;
					}
					else if(player==Player.computer)
					{
						board[row][column]=computerMark;
					}
				}
			}
		}
	}
	public void clearBoard()
	{
		initBoard();
	}
}
