package com.docostello.rockpaperscissors;

public class RockPaperScissor {

	private Move _move;
	public void setMove(Move move)
	{
		this._move = move;
	}
	
	public RockPaperScissor(Move decision)
	{
		this._move = decision;
	}
	
	
	enum Move
		{
			rock,
			paper,
			scissor
		}
	
	enum Player
		{
			player1,
			player2,
			none
		}
	public static Player isWinner(RockPaperScissor player1, RockPaperScissor player2)
	{
			Player isWinner = Player.none;
			if(player1._move == Move.rock && player2._move == Move.rock ||
					player1._move == Move.paper && player2._move == Move.paper ||
					player1._move == Move.scissor && player2._move == Move.scissor)
			{
				isWinner = Player.none;
			}
			if(player1._move == Move.rock && player2._move == Move.paper || 
					player1._move == Move.paper && player2._move == Move.rock)
			{
				isWinner = player1._move == Move.paper ? Player.player1 : Player.player2;
			}
			if(player1._move == Move.rock && player2._move == Move.scissor ||
					player1._move == Move.scissor && player2._move == Move.rock)
			{
				isWinner = player1._move == Move.rock ? Player.player1 : Player.player2;
			}
			if(player1._move == Move.paper && player2._move == Move.scissor ||
					player1._move == Move.scissor && player2._move == Move.paper)
			{
				isWinner = player1._move == Move.scissor ? Player.player1 : Player.player2;
			}
			
			return isWinner;
	}

}
