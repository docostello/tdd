package com.docostello.rockpaperscissors;

import static org.junit.Assert.*;

import org.junit.Test;

import com.docostello.rockpaperscissors.RockPaperScissor.Move;
import com.docostello.rockpaperscissors.RockPaperScissor.Player;

public class RockPaperScissorsTest {

	@Test
	public void test() {
		RockPaperScissor p1 = new RockPaperScissor(Move.rock);
		RockPaperScissor p2 = new RockPaperScissor(Move.paper);
		Player player = RockPaperScissor.isWinner(p1, p2);
		assertTrue(player == Player.player2);
		p1.setMove(Move.scissor);
		p2.setMove(Move.paper);
		player = RockPaperScissor.isWinner(p1, p2);
		assertTrue(player == Player.player1);
		p1.setMove(Move.scissor);
		p2.setMove(Move.scissor);
		player = RockPaperScissor.isWinner(p1, p2);
		assertTrue(player == Player.none);
		
	}

}
